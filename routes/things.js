var app = require('express')();

var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27000/';

//get all things
app.get('/', function(req, res, next) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("bankofthings");
        dbo.collection('things').aggregate([
            { $lookup:
                    {
                        from: 'users',
                        localField: 'history.user',
                        foreignField: '_id',
                        as: 'user_details'
                    }
            }
        ]).toArray(function(err, result) {
            if (err) throw err;
            res.json(result)
            db.close();
        });
    });

});

//get things borrowed by user, using user id
app.get('/borrowed', function(req, res, next) {
    let user = (req.query.user);
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("bankofthings");
        dbo.collection('things').find({ "history.user": ObjectId(user) }  ).toArray(function (err, result) {
            if (err) throw err;
            res.json(result)
            db.close();
        });
    });
});

//update thing history: update status to borrowed and add user id
app.get('/update_history_borrow', function(req, res, next) {
    let user_id = (req.query.user_id);
    let thing_id = (req.query.thing_id);
    let date_borrowed = (req.query.date_borrowed);
    let date_return = (req.query.date_return);
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("bankofthings");
        dbo.collection('things').updateOne(
            { "_id": ObjectId(thing_id) },
            {
                "$push": {
                    "history": {
                            date: date_borrowed,
                            status: 'borrowed',
                            user: user_id,
                            return_date: date_return
                        }
                }
                },
            function(err, result){
                        if (err) throw err;
                        res.json(result)
                        db.close();
                      }
        )


    });
});

//update thing history: update status to in stock
app.get('/update_history_instock', function(req, res, next) {
    let thing_id = (req.query.thing_id);
    let date = (req.query.date_borrowed);
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("bankofthings");
        dbo.collection('things').updateOne(
            { "_id": ObjectId(thing_id) },
            {
                "$push": {
                    "history": {
                        date: date,
                        status: 'in stock'
                    }
                }
            },
            function(err, result){
                if (err) throw err;
                res.json(result)
                db.close();
            }
        )


    });
});

//send the angular app: things controller
app.get('/ngThings.js', function(req, res) {
    res.setHeader('Content-Type', 'application/javascript');
    res.sendFile( __dirname.substring(0, __dirname.length - 7) + '/controllers' + '/ngThings.js');
});

//send the view to show things
app.get('/showThings', function(req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.sendFile( __dirname.substring(0, __dirname.length - 7) + '/views' + '/ngThings.html');
});

//send the angular app: options controller
app.get('/ngOptions.js', function(req, res) {
    res.setHeader('Content-Type', 'application/javascript');
    res.sendFile( __dirname.substring(0, __dirname.length - 7) + '/controllers' + '/ngOptions.js');
});

//send the view to show options
app.get('/options', function(req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.sendFile( __dirname.substring(0, __dirname.length - 7) + '/views' + '/ngOptions.html');
});

module.exports = app;