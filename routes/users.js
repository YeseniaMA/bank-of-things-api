var app = require('express')();

var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27000/';

//get all users
app.get('/', function(req, res, next) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("bankofthings");
    dbo.collection('users').aggregate([
      { $lookup:
            {
              from: 'things',
              localField: 'things.id',
              foreignField: '_id',
              as: 'things_details'
            }
      }
    ]).toArray(function(err, result) {
      if (err) throw err;
      res.json(result)
      db.close();
    });
  });

});

//get by name
app.get('/user', function(req, res, next) {
  let fname = (req.query.name);
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("bankofthings");
    dbo.collection('users').aggregate([
      { $match : { name : fname } },
      { $lookup:
            {
              from: 'things',
              localField: 'things.id',
              foreignField: '_id',
              as: 'things_details'
            }
      }
    ]).toArray(function(err, result) {
      if (err) throw err;
      res.json(result[0])
      db.close();
    });
  });
});

//get by id
app.get('/user', function(req, res, next) {
    let id = (req.query.id);
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("bankofthings");
        dbo.collection('users').aggregate([
            {$match: {_id: id}},
            {
                $lookup:
                    {
                        from: 'things',
                        localField: 'things.id',
                        foreignField: '_id',
                        as: 'things_details'
                    }
            }
        ]).toArray(function (err, result) {
            if (err) throw err;
            res.json(result[0])
            db.close();
        });
    });
});

//send the angular app: users controller
app.get('/ngUsers.js', function(req, res) {
    res.setHeader('Content-Type', 'application/javascript');
    res.sendFile( __dirname.substring(0, __dirname.length - 7) + '/controllers' + '/ngUsers.js');
});

//send the view to show users
app.get('/showUsers', function(req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.sendFile( __dirname.substring(0, __dirname.length - 7) + '/views' + '/ngUsers.html');
});

module.exports = app;