optionsApp = angular.module('angOptionsApp', []);

optionsApp.controller('OptionsListController',  function($scope, $window, $http) {

    let URL_ALL_THINGS = "http://localhost:3003/things";
    let URL_ITEMS_BORROWED = "http://localhost:3003/things/borrowed";
    let URL_ALL_USERS = "http://localhost:3003/users";

    $scope.things = [];
    $scope.thingsBorrowed = [];
    $scope.users = [];

    // show and hide tables
    $scope.showTable = false;
    $scope.showTable1 = false;
    $scope.showval = false;
    $scope.showuserlist = false;

    $http.get(URL_ALL_THINGS).then(function(response) {

        $scope.things =  response.data;

    });


    $http.get(URL_ALL_USERS).then(function(response) {

        $scope.users =  response.data;

    });


    //function to show and hide tables
    $scope.isShowHide = function (param) {
        if (param == "show") {
            $scope.showval = true;
            $scope.showuserlist = false;
        }

        if (param == "showUsers") {
            $scope.showuserlist = true;
        }

        $scope.showTable = false;
        $scope.showTable1 = false;

    }

    //function gets items borrowed history by a user
    $scope.GetDetails = function(uid) {
        $http.get(URL_ITEMS_BORROWED + '?user=' + uid).then(function(response) {
            $scope.borrowedHistory = []
            $scope.thingsBorrowed =  response.data;
            console.log($scope.thingsBorrowed[0]._id);

     // show table of items borrowed and hide history of things owned table
            $scope.showTable1 = true;
            $scope.showTable = false;
        });
    }

    //function gets response of things owned by a user
    $scope.GetHistory = function(selectedThing) {
        $scope.ownedHistory = []
        $http.get(URL_ALL_THINGS).then(function(response) {
            $scope.things =  response.data;
            for (var key in $scope.things){
                if ($scope.things.hasOwnProperty(key)) {
                    if($scope.things[key].name === selectedThing) {
                        var historyofThing = {
                            history: $scope.things[key].history,
                            user_details: $scope.things[key].user_details
                        }
                    }
                }
            }

            $scope.ownedHistory.push(historyofThing);
            //show history of things owned table and hide items borrowed
            $scope.showTable = true;
            $scope.showTable1 = false;
        });

    }

});