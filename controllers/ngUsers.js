usersApp = angular.module('angUsersApp', []);

usersApp.controller('UsersListController',  function($scope, $window, $http) {

    let URL_ALL_USERS = "http://localhost:3003/users";


    $scope.users = [];
    $http.get(URL_ALL_USERS).then(function(response) {

        $scope.users =  response.data;

    });

});