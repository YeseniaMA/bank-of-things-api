thingsApp = angular.module('angThingsApp', []);

thingsApp.controller('ThingsListController',  function($scope, $window, $http) {

    let URL_ALL_THINGS = "http://localhost:3003/things";

    $scope.things = [];

    $http.get(URL_ALL_THINGS).then(function(response) {

        $scope.things =  response.data;

    });
});